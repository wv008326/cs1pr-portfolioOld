#include <common.h>

static void touch(Entity *other);

void initSpike(char* line) {
	Entity *e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;
	
	sscanf(line, "%*s %f %f", &e->x, &e->y);

	e->health = 1;
	e->h = 61;
	e->w = 41;
	e->texture = loadTexture("gfx/spike.png");
	e->flags = EF_WEIGHTLESS;

	e->touch = touch;
}

static void touch(Entity* other) {
	if (self->health > 0 && other == player) {
		self->health = 0;

		if (player->life > 0) {
			player->life--;
		}
	}
}






