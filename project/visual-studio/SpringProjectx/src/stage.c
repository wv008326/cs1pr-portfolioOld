/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static void logic(void);
static void draw(void);
static void drawHud(void);
static void TimerUpdate(void);
SDL_TimerID timer;

void initStage(void)
{
	app.delegate.logic = logic;
	app.delegate.draw = draw;

	memset(&stage, 0, sizeof(Stage));

	stage.entityTail = &stage.entityHead;

	initEntities();

	initPlayer();

	initMap();

	SDL_TimerID timer = SDL_AddTimer(1000, TimerUpdate, NULL);
}

static void TimerUpdate() {
	stage.time = SDL_GetTicks();
	stage.Seconds = stage.time / 1000 % 60;
	stage.Minutes = stage.time / 1000 / 60;

	}
	


static void logic(void)
{
	doPlayer();

	doEntities();

	doCamera();
}

static void draw(void)
{
	SDL_SetRenderDrawColor(app.renderer, 0, 128, 255, 0);
	SDL_RenderFillRect(app.renderer, NULL);

	drawMap();

	drawEntities();

	drawHud();
}

static void drawHud(void)
{
	TimerUpdate();
	SDL_Rect r;

	r.x = 0;
	r.y = 0;
	r.w = SCREEN_WIDTH;
	r.h = 35;

	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 196);
	SDL_RenderFillRect(app.renderer, &r);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

	drawText(SCREEN_WIDTH / 20 - 45, 5, 255, 255, 255, TEXT_LEFT, "TIME: %d MINUTES %d SECONDS", stage.Minutes, stage.Seconds);

	drawText(SCREEN_WIDTH - 5, 5, 255, 255, 255, TEXT_RIGHT, "PIZZA %d/%d", stage.pizzaFound, stage.pizzaTotal);

	//drawText(SCREEN_WIDTH / 2, 5, 255, 255, 255, TEXT_CENTER, "%f/%f", player->x, player->y);

	drawText(SCREEN_WIDTH / 2, 5, 255, 255, 255, TEXT_CENTER, "Lives: %d", player->life);

	if (player->life == 0) {
		exit(1);
	}

	if (stage.pizzaFound == stage.pizzaTotal) {
		stage.finalMinutes = stage.Minutes;
		stage.finalSeconds = stage.Seconds;
		drawText(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 153, 153, 0, TEXT_CENTER, "FINISHED IN %d MINUTES AND %d SECONDS", stage.finalMinutes, stage.finalSeconds);
		
	}


	
}
